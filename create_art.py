import numpy as np
from PIL import Image

N_GENERATIONS = 30

IMG_SIZE = 512  # Dimension: IMG_SIZE X IMG_SIZE
IMG_DIVISION = 512  # Minimum is 2, maximum is IMG_SIZE

PATH = 'init_img/lisa.jpg'


def getMatrixFromImage(path):
    pixels = Image.open(path).convert('RGB')
    return np.asarray(pixels, dtype=np.uint8)


def showImageFromMatrix(array, save=False):
    img = Image.fromarray(array, 'RGB')
    img.show()
    if save:
        img.save('final_img/final.png')


def getFitness(img):
    ans = np.zeros(len(img))
    for i in range(len(img)):
        if sum(sum(img[i])) > 666:
            ans[i] = 1
        else:
            ans[i] = -1
    return ans


def mutation(img, mut, imgLen):
    kid = np.copy(img)
    randStart = np.random.randint(0, IMG_SIZE - imgLen - 1)
    mutSelected = mut[randStart: randStart + imgLen]
    for fitness, i in zip(getFitness(img), range(imgLen)):
        if fitness > 0:
            kid[i] += mutSelected[i] // 30
        else:
            kid[i] -= mutSelected[i] // 30
    return kid


def makeKid(img, mut):
    kids = np.copy(img)
    imgLen = IMG_SIZE // IMG_DIVISION
    for i in range(IMG_DIVISION):
        kids[i * imgLen: (i + 1) * imgLen] = mutation(img[i * imgLen:(i + 1) * imgLen], mut, imgLen)
    return kids


def main():
    mutationArr = getMatrixFromImage(PATH)
    initArr = getMatrixFromImage(PATH)
    for i in range(N_GENERATIONS):
        initArr = makeKid(initArr, mutationArr)
    showImageFromMatrix(initArr, True)


if __name__ == '__main__':
    main()
