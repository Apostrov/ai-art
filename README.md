# B17-2 Amir Subaev
> Introduction to AI Spring 2019
### HOWTO
1. Open folder with project
2. `pip install -r requirements.txt`
3. `python create_art.py`

or use `jupyter notebook` and start `create_art.ipynb`

### Change image
To change the picture, change the variable PATH to the picture path